# Terraform Gitlab Module

Opinionated Terraform module for managing GitLab resources.

When you start a new project you will be given a subgroup under https://gitlab.com/genomicsengland/ which you will will then manage (hopefully using this module!).

This module handles the heavy lifting of setting up repos in subgroups as you need, it will also connect your repo to Jira and Slack.

## Standards

All our production repos should have the same settings, as such we can just use variables for most settings with boilerplate code for each project. If we did require further customization per project then we can just change from a variable to a value.

This module sets a variety of settings to match the agreed [Git Policy](https://cnfl.extge.co.uk/pages/viewpage.action?pageId=132294732), these can be changed using standard terraform variables.

## Caveats

Unfortunately the GitLab provider for Terraform does not cover all out needs so some manual work per project is needed after running Terraform but this gets us 80% of the way there. This is documented [here](https://cnfl.extge.co.uk/display/CLOUD/GitLab+Terraform)

GitLab doesn't currently have the concept of a [service/bot/automation user](https://gitlab.com/gitlab-org/gitlab-ee/issues/6883) so you will need to use a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). It is not suggested to put these into CI/CD as this gives full API access to anything your GitLab user has access to, this includes any personal/private repos you have on GitLab.

## Usage

### Pre-Requisites

1. Be an Owner of the subgroup you wish to manage in GitLab
1. A [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with full permissions
1. The Jira `gitlabapi` user's password
1. Terraform

### Add Data Source For Your Subgroup

```
data "gitlab_group" "your-subgroup" {
  group_id = "12345678"
}
```

### Create Subgroup

```
resource "gitlab_group" "your-sub-subgroup" {
  name        = "Your Subgroup Name"
  path        = "your-sub-subgroup"
  parent_id   = "${data.gitlab_group.your-subgroup.id}"
  description = ""
}
```

### Create Repo In New Subgroup

```
module "core_shared-baseline" {
  # This will pull the module directly from gitlab, the `ref=2019.11.01`
  # references a git tag.
  source = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/gitlab?ref=2019.11.01"

  name         = "Your Repo Name"
  path         = "Your Repo Name"
  namespace_id = "${gitlab_group.your-sub-subgroup.id}"
  description  = "Terraform baseline for the Core/Shared account"

  jira_password    = "password" # this should come from a runtime variable, not saves in the code!
  jira_project_key = "CLOUD"

  slack_webhook               = "https://hooks.slack.com/services/1234/5678"
  slack_channel_merge_request = "your_channel_name"
  slack_channel_push          = "your_channel_name"
  slack_channel_pipeline      = "your_channel_name"

  only_allow_merge_if_pipeline_succeeds = true

  atlantis_webhook = {
    url: "https://atlantis.example.com/events",
    token: "abcd",
    }
  }
```
