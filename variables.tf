######## Required

# Required parameters are parameters which are too specific to a particular
# repository or team for the module to provide sensible defaults.

variable "name" {
  description = "Full name of the repository"
  type        = string
}

variable "path" {
  description = "URL-friendly path of the repository"
  type        = string
}

variable "description" {
  description = "Description for the repository"
  type        = string
}

variable "namespace_id" {
  description = "Namespace ID the repository is part of"
  type        = string
}

variable "jira_password" {
  description = "Password for Jira"
  type        = string
}

variable "jira_project_key" {
  description = "Key of the Jira project"
  type        = string
}

variable "slack_webhook" {
  description = "Slack webhook"
  type        = string
}

variable "slack_channel_merge_request" {
  description = "Slack channel for notifications on merge"
  type        = string
}

variable "slack_channel_push" {
  description = "Slack channel for notifications on push"
  type        = string
}

variable "slack_channel_pipeline" {
  description = "Slack channel for notifications on pipeline events"
  type        = string
}

######## Optional

# Optional parameters are parameters that should be common to repositories
# within the organization. The module tries to provide sensible defaults for
# them, but a user can always override them.

variable "default_branch" {
  default     = "main"
  description = "Default branch of the repository"
  type        = string
}

variable "issues_enabled" {
  default     = false
  description = "Whether issues are enabled"
  type        = bool
}

variable "approvals_before_merge" {
  default     = 1
  description = "Number of approvals required before merge"
  type        = number
}

variable "wiki_enabled" {
  default     = false
  description = "Whether the wiki is enabled"
  type        = bool
}

variable "snippets_enabled" {
  default     = false
  description = "Whether snippets are enabled"
  type        = bool
}

variable "container_registry_enabled" {
  default     = false
  description = "Whether container registry is enabled"
  type        = bool
}

variable "visibility_level" {
  default     = "private"
  description = "Visibility level of the repository"
  type        = string
}

variable "merge_method" {
  default     = "ff"
  description = "Merge method for the repository"
  type        = string
}

variable "shared_runners_enabled" {
  default     = false
  description = "Whether shared runners are enabled"
  type        = bool
}

variable "initialize_with_readme" {
  default     = true
  description = "Should it initialize with a README.md"
  type        = bool
}

variable "only_allow_merge_if_pipeline_succeeds" {
  default     = true
  description = "Whether to only allow merge if pipeline succeeds"
  type        = bool
}

variable "only_allow_merge_if_all_discussions_are_resolved" {
  default     = true
  description = "Whether to only allow merge if all discussions are resolved"
  type        = bool
}

variable "branch_to_protect" {
  default     = "master"
  description = "Branch to protect"
  type        = string
}

variable "push_access_level" {
  default     = "maintainer"
  description = "Access level required to push"
  type        = string
}

variable "merge_access_level" {
  default     = "maintainer"
  description = "Access level required to merge"
  type        = string
}

variable "jira_url" {
  default     = "https://jira.extge.co.uk/"
  description = "URL of the Jira instance"
  type        = string
}

variable "jira_username" {
  default     = "gitlabapi"
  description = "Username for Jira"
  type        = string
}

variable "jira_issue_transition_id" {
  default     = "31,41"
  description = "ID of a transition that moves issues to a closed state"
  type        = string
}

variable "slack_username" {
  default     = "GitLab"
  description = "Slack username"
  type        = string
}

variable "slack_merge_request_events" {
  default     = true
  description = "Whether to notify Slack on merge"
  type        = bool
}

variable "slack_tag_push_events" {
  default     = false
  description = "Whether to notify Slack on push"
  type        = bool
}

variable "slack_pipeline_events" {
  default     = true
  description = "Whether to notify Slack on pipeline events"
  type        = bool
}

variable "groups_to_share_with" {
  default     = {}
  description = "Map of groups (maps) to share a project with"
  type        = map(map(string))
}

variable "atlantis_webhook" {
  default = {
    url : ""
  }
  description = "Atlantis webhook integration"
  type        = map
}

variable "pipeline_schedules" {
  description = "A list of objects defining pipeline schedules"
  type = list(object({
    description = string
    ref         = string
    cron        = string
  }))
  default = []
}
