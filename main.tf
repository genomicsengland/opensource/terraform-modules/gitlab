resource "gitlab_project" "repository" {
  name         = var.name
  path         = var.path
  description  = var.description
  namespace_id = var.namespace_id

  default_branch             = var.default_branch
  issues_enabled             = var.issues_enabled
  approvals_before_merge     = var.approvals_before_merge
  wiki_enabled               = var.wiki_enabled
  snippets_enabled           = var.snippets_enabled
  container_registry_enabled = var.container_registry_enabled
  visibility_level           = var.visibility_level
  merge_method               = var.merge_method
  shared_runners_enabled     = var.shared_runners_enabled
  initialize_with_readme     = var.initialize_with_readme

  only_allow_merge_if_pipeline_succeeds            = var.only_allow_merge_if_pipeline_succeeds
  only_allow_merge_if_all_discussions_are_resolved = var.only_allow_merge_if_all_discussions_are_resolved
}

resource "gitlab_project_share_group" "repository" {
  for_each     = var.groups_to_share_with
  project_id   = gitlab_project.repository.id
  group_id     = each.value.group_id
  access_level = each.value.group_access_level
}

resource "gitlab_branch_protection" "repository" {
  project            = gitlab_project.repository.id
  branch             = var.branch_to_protect
  push_access_level  = var.push_access_level
  merge_access_level = var.merge_access_level
}

resource "gitlab_service_jira" "repository" {
  project                  = gitlab_project.repository.id
  url                      = var.jira_url
  username                 = var.jira_username
  password                 = var.jira_password
  jira_issue_transition_id = var.jira_issue_transition_id
  project_key              = var.jira_project_key
}

resource "gitlab_service_slack" "repository" {
  project  = gitlab_project.repository.id
  webhook  = var.slack_webhook
  username = var.slack_username

  merge_requests_events = var.slack_merge_request_events
  merge_request_channel = var.slack_channel_merge_request

  tag_push_events  = var.slack_tag_push_events
  tag_push_channel = var.slack_channel_push

  pipeline_events  = var.slack_pipeline_events
  pipeline_channel = var.slack_channel_pipeline

  notify_only_broken_pipelines = true
}

resource "gitlab_project_hook" "repository_atlantis" {
  count   = var.atlantis_webhook.url != "" ? 1 : 0
  project = gitlab_project.repository.id
  url     = var.atlantis_webhook.url

  token                   = lookup(var.atlantis_webhook, "token", null)
  enable_ssl_verification = lookup(var.atlantis_webhook, "enable_ssl_verification", true)
  push_events             = lookup(var.atlantis_webhook, "push_events", true)
  issues_events           = lookup(var.atlantis_webhook, "issues_events", false)
  merge_requests_events   = lookup(var.atlantis_webhook, "merge_requests_events", true, )
  tag_push_events         = lookup(var.atlantis_webhook, "tag_push_events", false)
  note_events             = lookup(var.atlantis_webhook, "note_events", true)
  job_events              = lookup(var.atlantis_webhook, "job_events", false)
  pipeline_events         = lookup(var.atlantis_webhook, "pipeline_events", false)
  wiki_page_events        = lookup(var.atlantis_webhook, "wiki_page_events", false)
}

resource "gitlab_pipeline_schedule" "repository" {
  count = length(var.pipeline_schedules)

  project       = gitlab_project.repository.id
  description   = lookup(element(var.pipeline_schedules, count.index), "description", null)
  ref           = lookup(element(var.pipeline_schedules, count.index), "ref", null)
  cron          = lookup(element(var.pipeline_schedules, count.index), "cron", null)
  cron_timezone = lookup(element(var.pipeline_schedules, count.index), "cron_timezone", "UTC")
  active        = true
}
